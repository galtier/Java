# Image for the Java course

# user = abc, password = abc
# sudo: no password

# Build:
# docker build -t java .

# Run:
# docker run --rm --detach --privileged --publish 3000:3000 --publish 3001:3001 --env TITLE=Java java:latest
# or, if you want to "mount" a host directory to the /home/abc/PersistentDir directory in the container:
# docker run --rm --detach --privileged --publish 3000:3000 --publish 3001:3001 --env TITLE=Java --volume YOUR_CHOICE_OF_PATH:/home/abc/PersistentDir:rw java:latest
# and to use your host user id and host group id (XXXX is given by id -u and YYYY by id -g):
# docker run --rm --detach --privileged --publish 3000:3000 --publish 3001:3001 --env TITLE=Java --env PUID=XXXX --env PGID=YYYY --volume YOUR_CHOICE_OF_PATH:/home/abc/PersistentDir:rw java:latest

# Connect to:
# Use a web browser and visit http://127.0.0.1:3000/

FROM	lscr.io/linuxserver/webtop:ubuntu-xfce 

# Give the abc user a regular home and nice prompt
RUN	mkdir /home/abc && mkdir /home/abc/Downloads && mkdir /home/abc/PersistentDir && chown -R abc:abc /home/abc && usermod -d /home/abc abc
RUN	echo "PS1='\u@graoully:\w\$ '" >> /home/abc/.bashrc
RUN 	echo "alias ls='ls --color=auto'" >> /etc/bash.bashrc
ENV	HOME=/home/abc

# Install a few utilities
RUN	apt-get update && apt-get upgrade -y && \
	apt-get install -y zip unzip git gedit tree iputils-ping wget net-tools evince texlive-fonts-extra maven retext

# get rid of gedit warning messages
RUN	echo "#!/bin/bash" > /usr/bin/gedit-null && echo "/usr/bin/gedit \""\$\@\"" 2>/dev/null" >> /usr/bin/gedit-null && chmod 755 /usr/bin/gedit-null
RUN	echo "alias gedit=/usr/bin/gedit-null" >> /etc/bash.bashrc 

# Add bookmarks to Firefox
COPY	profiles.ini /home/abc/.mozilla/firefox/profiles.ini
COPY	prefs.js /home/abc/.mozilla/firefox/ial.default-release/prefs.js
COPY	favicons.sqlite /home/abc/.mozilla/firefox/ial.default-release/favicons.sqlite
COPY	places.sqlite /home/abc/.mozilla/firefox/ial.default-release/places.sqlite    
RUN	chown -R abc:abc /home/abc/.mozilla/

# xfce-terminal is nice to script tabs and stuff
RUN	apt-get install -y xfce4-terminal

# Install Eclipse JEE version
RUN	wget https://ftp.fau.de/eclipse/technology/epp/downloads/release/2023-09/R/eclipse-java-2023-09-R-linux-gtk-x86_64.tar.gz \
&&	zcat eclipse-java-2023-09-R-linux-gtk-x86_64.tar.gz | tar xvf - \
&&	mv eclipse /opt \
&&	rm eclipse-java-2023-09-R-linux-gtk-x86_64.tar.gz
ENV	PATH=$PATH:/opt/eclipse
# add Eclipse item to start menu:
COPY	eclipse.desktop /usr/share/applications/eclipse.desktop
RUN	chmod a+r /usr/share/applications/eclipse.desktop

RUN	apt-get install -y openjdk-11-jdk
ENV	JAVA_HOME=/usr/lib/jvm/default-java
RUN	export JAVA_HOME
ENV	PATH=$PATH:$JAVA_HOME
RUN	export PATH


RUN	apt autoremove && apt autoclean
RUN	echo "export PATH=$PATH" > /etc/environment


# Clean up
RUN	apt-get clean && \
	rm -rf \
	/tmp/* \
	/var/lib/apt/lists/* \
	/var/tmp/*






