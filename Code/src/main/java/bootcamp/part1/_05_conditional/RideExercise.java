package bootcamp.part1._05_conditional;

public class RideExercise {

	public static void main(String[] args) {
		new RideExercise();
	}

	RideExercise() {
		System.out.println(getFare(false, 9)); // expected result: 9
		System.out.println(getFare(true, 9)); // expected result: 8.1
		System.out.println(getFare(true, 5)); // expected result: 4.5
	}

	double getFare(boolean isMember, int age) {
		if (!isMember) {
			return age;
		} else {
			double bonus = (double) age / 10;
			return age - bonus;
		}
	}

}
