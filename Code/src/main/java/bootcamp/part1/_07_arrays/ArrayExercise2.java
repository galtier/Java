package bootcamp.part1._07_arrays;

import java.util.Arrays;

public class ArrayExercise2 {

	public static void main(String[] args) {
		new ArrayExercise2();
	}

	ArrayExercise2() {
		int[] a = {1, 2, 3, 4};   // shortcut to declaration + creation + assignment
		display(a);
	}

	void display(int[] t) {
		// The beginner way:
		System.out.print("[");
		for (int i = 0; i< t.length-1; i++) {
			System.out.print(t[i] + ", ");
		}
		System.out.print(t[t.length-1] + "]\n");
		// The easy way (requires to import the Arrays package):
		System.out.println(Arrays.toString(t));	
	}	

}
