package bootcamp.part1._06_loops;

public class PyramidExercise {

	public static void main(String[] args) {
		new PyramidExercise();
	}
	
	PyramidExercise() {
		pyramid(4);
	}
	
	void pyramid(int n) {
		for (int ligne=1; ligne<=n; ligne++) {
			for (int colonne=ligne; colonne>=1; colonne--) {
				System.out.print(colonne);
			}
			System.out.print("\n");
		}
	}

}
