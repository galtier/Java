package bootcamp.part1._02_variables;

public class PrimitiveVarDemo {

	public static void main(String[] args) {
		new PrimitiveVarDemo();
	}
	
	PrimitiveVarDemo() {
		int nb; // declare variable type and name
		nb = 3; // assign value ("affectation" en français)
		
		int n = 4; // declaration and value assignment in one line
		
		boolean isGood = false; // !!! "False" in Python, but "false" in Java
		
		char a = 'e';
		
		
		// examples of compilation errors
		/*
		d = "a";       // d does not exist
		char c = "a";  // "a" is not a char but a String
		int m;
		System.out.println(m);    // m may not have been initialized
		*/
		
		// example of runtime error (/ by zero)
		/*
		int i = 0;
		int q = 3/i;
		*/

	}
}
