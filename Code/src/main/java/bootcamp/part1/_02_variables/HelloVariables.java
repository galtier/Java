package bootcamp.part1._02_variables;

public class HelloVariables {

	public static void main(String[] args) {
		new HelloVariables();
	}
	
	HelloVariables() {
		System.out.println("Hello world!");
		char a = 'e';
		char c;
		c = 'a';  // the 'a' letter
		c = a;    // content of the variable named a
		c = '\n'; // 2 characters \ and n but it represents a single, non-printable, character: new line
		//c = null; // forbidden: c is a char, primitive type, and cannot hold the null value
		//c = "a"; // forbidden: with the double quotes, "a" is a string of characters, reduced to a single character, but a string nonetheless
		//c = ''; // forbidden: there no such character
		//c = b; // forbidden: there is no variable named 'b'
	}
}
