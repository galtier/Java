package bootcamp.part1._03_methods;

public class MethodNoParamNoReturn {

	public static void main(String[] args) {
		new MethodNoParamNoReturn();
	}
	
	MethodNoParamNoReturn() {
		hello();
	}
	
	void hello() {
		System.out.println("hello");
	}
}
