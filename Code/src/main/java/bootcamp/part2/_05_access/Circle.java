package bootcamp.part2._05_access;

class Circle {

	// version 1
	/*
	int radius; 

	Circle(int radius) {
		this.radius = radius;
	}

	double getPerimeter() {
		return 2*Math.PI*radius;
	}
	 */

	// version 2
	/*
	int diameter;

	Circle(int radius) {
		this.diameter = radius*2;
	}

	double getPerimeter() {
		return Math.PI*diameter;
	}
	 */

	// version 3
	/*
	private int radius;

	Circle(int radius) {
		this.radius = radius;
	}

	double getPerimeter() {
		return 2*Math.PI*radius;
	}

	public int getRadius() {
		return radius;
	}
	 */

	// version 4
	private double surface;

	Circle(int radius) {
		this.surface = Math.PI * Math.pow(radius, 2);
	}

	double getPerimeter() {
		return 2*Math.PI*Math.sqrt(surface/Math.PI);
	}

	public int getRadius() {
		return (int)(Math.sqrt(surface/Math.PI));
	}
}
