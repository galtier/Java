package bootcamp.part2._05_access;

public class MainCircle {

	public static void main(String[] args) {
		new MainCircle();
	}

	MainCircle() {
		Circle c = new Circle(3);
		
		// version 1
		//System.out.println("The perimeter of a circle of radius " + c.radius + " is " + c.getPerimeter());
		
		// version 2
		//System.out.println("The perimeter of a circle of radius " + (c.diameter/2) + " is " + c.getPerimeter());
		
		// versions 3 & 4
		System.out.println("The perimeter of a circle of radius " + c.getRadius() + " is " + c.getPerimeter());
		
		// version 4: The field Circle.surface is not visible	
		//System.out.println("The perimeter of a circle of radius " + c.surface + " is " + c.getPerimeter());					
	}
}
