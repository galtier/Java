package bootcamp.part2._08_abstract;

abstract class Shape {
	   protected abstract double getArea();
	   protected abstract double getPerimeter();
	}