package bootcamp.part2._08_abstract;

public class MainAnimal {
	public static void main(String[] args) {
		Animal[] pets;
		pets = new Animal[3];
		pets[0] = new Dog(3);
		pets[1] = new Cat(7);
		pets[2] = new Dog(12);
		for (int i=0; i<pets.length; i++) {
			System.out.println(pets[i].getSound());
		}
	}
}
