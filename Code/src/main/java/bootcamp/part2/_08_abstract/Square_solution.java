package bootcamp.part2._08_abstract;

public class Square_solution extends Shape {

	protected double side;
	
	public Square_solution(double side) {
		super();
		this.side = side;
	}

	@Override
	protected double getArea() {
		return side*side;
	}

	@Override
	protected double getPerimeter() {
		return side*4;
	}

}
