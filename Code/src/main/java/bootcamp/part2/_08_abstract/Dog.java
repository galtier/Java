package bootcamp.part2._08_abstract;

public class Dog extends Animal {

	public Dog(int age) {
		super(age);
	}

	@Override
	String getSound() {
		if (age > 10)
			return "WOOF!";
		else
			return "Waof!";
	}

}
