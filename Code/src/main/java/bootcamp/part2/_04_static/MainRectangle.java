package bootcamp.part2._04_static;

class MainRectangle {

	public static void main(String[] args) {
		new MainRectangle();
	}
	
	MainRectangle() {
	    Rectangle r1, r2, r3;
	    System.out.println("Rectangle.nbRectangles = " + Rectangle.nbRectangles);
	    r1 = new Rectangle(4.0, 6.0);
	    System.out.println("Rectangle.nbRectangles = " + Rectangle.nbRectangles);
	    System.out.println("r1.nbRectangles = " + r1.nbRectangles);
	    r2 = new Rectangle(1.0, 3.0);
	    System.out.println("Rectangle.nbRectangles = " + Rectangle.nbRectangles);
	    System.out.println("r1.nbRectangles = " + r1.nbRectangles);
	    System.out.println("r2.nbRectangles = " + r2.nbRectangles);
	}
}
