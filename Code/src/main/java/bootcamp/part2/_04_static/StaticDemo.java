package bootcamp.part2._04_static;

class StaticDemo {
	public static void main(String[] args) { 
		Machine machA = new Machine();
		Machine machB = new Machine();
		Machine machC = machA;
		System.out.println(machC.foo + " " + machC.bar);
	}
}

class Machine {
	public static int foo = 0;
	public int bar;
	public Machine(){
		foo++;
		bar = foo;
	}
}

