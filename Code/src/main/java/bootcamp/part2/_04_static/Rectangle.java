package bootcamp.part2._04_static;

class Rectangle {

	// attributes:
	double width;
	double length;
	static int nbRectangles = 0;

	// constructor:
	Rectangle(double width, double length) {
		this.width = width;
		this.length = length;
		nbRectangles++;
	}

	// methods that read attributes:
	double getArea() {
		return (length * width);
	}
	double getPerimeter() {
		return 2 * (length + width);
	}
	void printDescription() {
		System.out.println("width = " + width + ", length = " + length);
	}

	// method that changes attributes: 
	void resize(double coefficient) {
		length = length * coefficient;
		width = width * coefficient;
	}
}
