package bootcamp.part2._06_inheritance;

public class Teacher extends Person {
    protected String specialty;
    Teacher(String name, String specialty) {
	    super(name);
	    this.specialty = specialty;
    }
    void enter() {
	    isIn = true;
	    System.out.print(name + " (teacher) stepped in. ");
    }	
}
