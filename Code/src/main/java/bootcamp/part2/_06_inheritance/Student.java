package bootcamp.part2._06_inheritance;

public class Student extends Person {
	protected int gradYear;
	Student(String name, int gradYear) {
		super(name);
		this.gradYear = gradYear;
	}	
}
