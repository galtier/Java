package bootcamp.part2._06_inheritance;
public class Bob extends Hat {
	public Bob(){
		x++;
	}
	public Bob(int i)  {
		this();
		x=x+i;
	}
	public Bob(String s){
		x--; 
	}
}