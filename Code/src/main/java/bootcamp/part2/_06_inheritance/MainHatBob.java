package bootcamp.part2._06_inheritance;

public class MainHatBob {

	public static void main(String[] args) {
	    Bob b1 = new Bob();
	    Bob b2 = new Bob(2003);
	    Bob b3 = new Bob("Hello");
	    System.out.println(b1.x + " " + b2.x + " " + b3.x);
	}
}
