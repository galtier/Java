/**
 * A Dog is an Animal, with a size (int).
 * Write the code for the Dog class. 
 * If the dog is larger than 10, it greets with "WOOOF!"; otherwise, it greets by printing "Waof!".
 */
package bootcamp.part2._06_inheritance;

public class MainAnimal {

	public static void main(String[] args) {
		int age = 3;
		int size = 20;
		Dog d = new Dog(age, size);
		d.greet(); // expected print: WOOOF!
		System.out.println(d.getAge());
	}
}
