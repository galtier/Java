package bootcamp.part2._06_inheritance;

public class Animal {

	private int age;

	protected int getAge() {
		return age;
	}

	protected void setAge(int age) {
		this.age = age;
	}
}
