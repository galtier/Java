package bootcamp.part2._02_object;

public class MainGeo {

	public static void main(String[] args) {
		Point p1;
		p1 = new Point(10, 10, "top");
		p1.printPosition();
	
		Rectangle r;
		r = new Rectangle(4, 6);
		Rectangle r2 = new Rectangle(4, 6);
		Rectangle r3 = r;
		r.printDescription();
		r3.resize(2);
		r3.printDescription();
	}

}
