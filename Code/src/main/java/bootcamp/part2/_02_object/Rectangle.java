package bootcamp.part2._02_object;

class Rectangle {

	// attributes:
	double width;
	double length;

	// constructor:
	Rectangle(double w, double l) {
		width = w;
		length = l;
	}

	// methods that read attributes:
	double getArea() {
		return (length * width);
	}
	double getPerimeter() {
		return 2 * (length + width);
	}
	void printDescription() {
		System.out.println("width = " + width + ", length = " + length);
	}

	// method that changes attributes: 
	void resize(double coefficient) {
		length = length * coefficient;
		width = width * coefficient;
	}
}
